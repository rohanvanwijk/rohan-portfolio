// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: '2024-04-03',
  devtools: { enabled: true },
  srcDir: 'src/',
  app: {
    head: {
      htmlAttrs: {
        lang: 'en'
      },
      title: 'Rohan van Wijk Frontend developer',
      meta: [
        {
          name: 'description',
          content: 'Rohan van Wijk is a frontend developer based in Amsterdam',
        },
        {
          name: 'theme-color',
          content: '#041E42',
        },
        {
          name: 'msapplication-TileColor',
          content: '#041E42',
        }
      ],
      link: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic',
        },
      ],
    },
  },
})